 # *format*, a Linux utility formatting a text read on stdin according to to French and English typographic rules.

Refer to [**libFormat**](https://gitlab.com/mubunt/libFormat) library.

## LICENSE
**format** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ format -h
format - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
format - Version 1.0.0

A utility formatting a text read on stdin according to to French and English
typographic rules.

Usage: format [OPTIONS]...

  -h, --help          Print help and exit
  -V, --version       Print version and exit
  -w, --width=NUMBER  Width of the final text
  -e, --english       English typographic rules  (default=off)
  -f, --french        French typographic rules  (default=off)
  -l, --left          Alignment to left  (default=off)
  -r, --right         Alignment to right  (default=off)
  -j, --justify       Justification of text  (default=off)

Exit: returns a non-zero status if an error is detected.

$ format -V
format - Copyright (c) 2020, Michel RIZZO. All Rights Reserved.
format - Version 1.0.0

$ text="Cosette était laide.     heureuse  , elle eut peut-être été jolie. Nous avons déjà esquissé cette petite figure sombre  . \
Cosette était maigre et blême. Elle avait prés de huit ans, on lui en eut donné à peine six. Ses grands yeux \
enfoncés dans une sorte d'ombre profonde étaient presque éteints à force d'avoir pleuré. Les coins de sa bouche \
avaient cette courbe de l'angoisse habituelle, qu'on observe chez les condamnés et chez les malades désespérés.\
Ses mains étaient, comme sa mère l'avait deviné,      « perdues d’engelures ». Le feu qui l'éclairait en ce moment \
faisait saillir les angles de ses os et rendait sa maigreur affreusement visible. Comme elle grelotait toujours, \
elle avait pris l'habitude de serrer ses deux genoux l'un contre l'autre."
$ echo $text |format --french --left --width=80
  Cosette était laide. Heureuse, elle eut peut-être été jolie. Nous avons déjà
esquissé cette petite figure sombre. Cosette était maigre et blême. Elle avait
prés de huit ans, on lui en eut donné à peine six. Ses grands yeux enfoncés dans
une sorte d'ombre profonde étaient presque éteints à force d'avoir pleuré. Les
coins de sa bouche avaient cette courbe de l'angoisse habituelle, qu'on observe
chez les condamnés et chez les malades désespérés. Ses mains étaient, comme sa
mère l'avait deviné, « perdues d’engelures ». Le feu qui l'éclairait en ce
moment faisait saillir les angles de ses os et rendait sa maigreur affreusement
visible. Comme elle grelotait toujours, elle avait pris l'habitude de serrer ses
deux genoux l'un contre l'autre. 
$ echo $text |format --french --left --width=80 | frame --text

+----------------------------------------------------------------------------------+
|   Cosette était laide. Heureuse, elle eut peut-être été jolie. Nous avons déjà   |
| esquissé cette petite figure sombre. Cosette était maigre et blême. Elle avait   |
| prés de huit ans, on lui en eut donné à peine six. Ses grands yeux enfoncés dans |
| une sorte d'ombre profonde étaient presque éteints à force d'avoir pleuré. Les   |
| coins de sa bouche avaient cette courbe de l'angoisse habituelle, qu'on observe  |
| chez les condamnés et chez les malades désespérés. Ses mains étaient, comme sa   |
| mère l'avait deviné, « perdues d’engelures ». Le feu qui l'éclairait en ce       |
| moment faisait saillir les angles de ses os et rendait sa maigreur affreusement  |
| visible. Comme elle grelotait toujours, elle avait pris l'habitude de serrer ses |
| deux genoux l'un contre l'autre.                                                 |
+----------------------------------------------------------------------------------+

$

```
## STRUCTURE OF THE APPLICATION
This section walks you through **format**'s structure. Once you understand this structure, you will easily find your way around in **format**'s code base.

``` bash
$ yaTree
./                       # Application level
├── examples/            # Examples directory (set of scripts)
│   ├── example_format   # 
│   ├── test01           # To run: ./test01 -a | -r | -l | -j
│   ├── test02           # To run: ./test02 -a | -r | -l | -j
│   ├── test03           # To run: ./test03 -a | -r | -l | -j
│   ├── test04           # To run: ./test04 -a | -r | -l | -j
│   ├── test05           # To run: ./test05 -a | -r | -l | -j
│   ├── test06           # To run: ./test06 -a | -r | -l | -j
│   ├── test07           # To run: ./test07 -a | -r | -l | -j
│   ├── test_kernel.bash # Subscript to centralize te call to 'format' utility
│   └── test_run.bash    # Script to generate the calls to 'format' utility with all options
├── src/                 # Source directory
│   ├── Makefile         # Makefile
│   ├── format.c         # Source file
│   └── format.ggo       # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md           # GNU General Public License markdown file
├── LICENSE.md           # License markdown file
├── Makefile             # Makefile
├── README.md            # ReadMe markdown file
├── RELEASENOTES.md      # Release Notes markdown file
└── VERSION              # Version identification text file

2 directories, 19 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd format
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd format
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - [**libFormat**](https://gitlab.com/mubunt/libFormat) library
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***