#! /bin/bash
#-------------------------------------------------------------------------------
function fatalerror {
	${CMD_ECHO} "Error: ${1}"
	exit 1
}
#-------------------------------------------------------------------------------
CMD_FRAME="frame"
CMD_ECHO="echo"
SCRIPT="\033[3mtest\033[0m"
#### Argument parsing
RULE="ENGLISH"
ALIGNMENT="RIGHT"
WIDTH=80
for i in "$@"; do
	case $i in
	-h|--help)
		echo
		echo -e "${SCRIPT} [-h |--help]		Display this help and exit"
		echo -e "${SCRIPT} [-w=|--width=n]		Width of the final text"
		echo -e "${SCRIPT} [-e|--english]		English typographic rules"
		echo -e "${SCRIPT} [-f|--french]		French typographic rules"
		echo -e "${SCRIPT} [-l|--left]		Alignment to left"
		echo -e "${SCRIPT} [-r|--right]		Alignment to right"
		echo -e "${SCRIPT} [-j|--justify]		Justification"
		echo
		exit 0
		;;
	-w=*|--width=*)
		WIDTH=`echo $i | sed 's/[-a-zA-Z0-9]*=//'`
		[ ! -z "${WIDTH##*[!0-9]*}" ] || fatalerror "Width argument is not a number!!!";
		;;
	-e|--english)
		RULE="--english" ;;
	-f|--french)
		RULE="--french" ;;
	-l|--left)
		ALIGNMENT="--left" ;;
	-r|--right)
		ALIGNMENT="--right" ;;
	-j|--justify)
		ALIGNMENT="--justify" ;;
	*)
		fatalerror "Unknown option!!!" ;;
	esac
done
#-------------------------------------------------------------------------------
echo "Rule=${RULE} Alignment=${ALIGNMENT} Width=${WIDTH}" | ${CMD_FRAME} --bold --block
format ${RULE} ${ALIGNMENT} --width=${WIDTH} <&0 | ${CMD_FRAME} --single
#format --rule=${RULE} --alignment=${ALIGNMENT} --width=${WIDTH} <&0
#echo "---"
#-------------------------------------------------------------------------------
