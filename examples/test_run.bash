#! /bin/bash
#-------------------------------------------------------------------------------
function fatalerror {
	echo
	echo -e "Error: Wrong launch!!!"
	echo
	echo -e "Usage: \033[1mtest\033[0m [ \033[3moption\033[0m ]"
	echo -e "  with option =	-ALL | -A | -all | -a"
	echo -e "  or		-LEFT | -L | -left | -l"
	echo -e "  or		-RIGHT | -R | -right | -r"
	echo -e "  or		-JUSTIFY | -J | -justify | -j"
	echo
	exit 1
}
#-------------------------------------------------------------------------------
if [ $# -eq 3 ]; then
	text="$1"
	width=$2
	case $3 in
		-ALL|-all|-A|-a)
			echo -e ${text} | ./test_kernel.bash --english --left --width=${width}
			echo -e ${text} | ./test_kernel.bash --english --right --width=${width}
			echo -e ${text} | ./test_kernel.bash --english --justify --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --left --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --right --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --justify --width=${width}
			;;
		-LEFT|-left|-L|-l)
			echo -e ${text} | ./test_kernel.bash --english --left --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --left --width=${width}
			;;
		-RIGHT|-right|-R|-r)
			echo -e ${text} | ./test_kernel.bash --english --right --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --right --width=${width}
			;;
		-JUSTIFY|-justify|-J|-j)
			echo -e ${text} | ./test_kernel.bash --english --justify --width=${width}
			echo -e ${text} | ./test_kernel.bash --french --justify --width=${width}
			;;
		*)
			fatalerror
			;;
	esac
else
	fatalerror
fi
#-------------------------------------------------------------------------------
