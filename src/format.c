//------------------------------------------------------------------------------
// Copyright (c) 2020, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: format
// A utility formatting a text read on stdin according to to French and English typographic rules.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "format_cmdline.h"
#include "txtFormat.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define error(fmt, ...) \
	do { fprintf(stderr, "ERROR: " fmt "\n", __VA_ARGS__); } while (0)
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info args_info;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_format(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	eformat_alignment alignment = LEFT;
	eformat_rule rule = ENGLISH;
	size_t width = 80;
	if (args_info.english_given + args_info.french_given == 2) {
		error("%s", "Multiple typographic rule options. Choose one.");
		goto EXIT;
	}
	if (args_info.left_given + args_info.right_given  + args_info.justify_given > 1) {
		error("%s", "Multiple alignment options. Choose one.");
		goto EXIT;
	}
	if (args_info.width_given) width = (size_t) args_info.width_arg;
	if (args_info.english_given) rule = ENGLISH;
	if (args_info.french_given) rule = FRENCH;
	if (args_info.left_given) alignment = LEFT;
	if (args_info.right_given) alignment = RIGHT;
	if (args_info.justify_given) alignment = JUSTIFY;
	//----  Go on --------------------------------------------------------------
	char *line = NULL;
	size_t len = 0;
	char *res;
	while (getline(&line, &len, stdin) != -1) {
		res = textformat(line, width, alignment, rule);
		if (res != NULL) {
			fprintf(stdout, "%s", res);
			free(res);
		}
	}
	free(line);
	//---- Exit ----------------------------------------------------------------
	cmdline_parser_format_free(&args_info);
	return EXIT_SUCCESS;
EXIT:
	cmdline_parser_format_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
