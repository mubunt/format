#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: format
# A utility formatting a text read on stdin according to to French and English typographic rules.
#-------------------------------------------------------------------------------
COPYRIGHT   = "2020"
DIRS        = src

include .make/topLevel.mk
#-------------------------------------------------------------------------------

